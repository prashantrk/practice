package Itinerary;

import java.util.HashMap;

public class ItineraryClass
{
    static HashMap<String,String> hashMap = new HashMap<String, String>();
    static String src ="Hubli";
    static String dest="Bangalore";

    static boolean checkForItinerary(String curCity, String dst)
    {
        if(curCity == null )
            return false;

        if(!curCity.equalsIgnoreCase(dst))
        {
            return checkForItinerary(hashMap.get(curCity), dst);
        }
        else {
            return true;
        }
    }
    public static void main(String args[])
    {
        hashMap.put("Hubli","Pune");
        hashMap.put("Pune","Bombay");
        hashMap.put("Bombay","Belgavi");
        hashMap.put("Belgavi","Dharwad");
        hashMap.put("Dharwad","Davanageri");
        hashMap.put("Davanageri","Bangalore");

        if(checkForItinerary("Hubli", "Banga"))
        {
            System.out.println("Available");
        }
        else
        {
            System.out.println("Not Available");
        }
    }

}
