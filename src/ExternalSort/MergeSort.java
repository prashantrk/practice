package ExternalSort;

import java.io.*;

public class MergeSort {

    final static int MAX_BUF_SIZE = 1000;
    final static int TOTAL_SIZE = 10000;
    final static int CHUNKS = TOTAL_SIZE / MAX_BUF_SIZE;
    static int[] buffer = new int[MAX_BUF_SIZE];
    final static String inputFilePath = "/home/dell/IdeaProjects/Practice/src/RandomNumbers.txt";
    final static String outputFilePath = "/home/dell/IdeaProjects/Practice/src/SortedNumbers.txt";
    static int fileIndex = 0;

    /*****************************************************************************************************************/
    public static int sortAndWrite() {
        fileIndex++;
        BufferedWriter writer = null;
        String intermediateFile = "interMediateFile" + fileIndex + ".txt";

        try {
            writer = new BufferedWriter(new FileWriter(intermediateFile));
            for (int indexI = 0; indexI < MAX_BUF_SIZE; indexI++) {
                for (int indexJ = 0; indexJ < MAX_BUF_SIZE; indexJ++) {
                    int temp = 0;
                    if (buffer[indexJ] > buffer[indexI]) {
                        temp = buffer[indexJ];
                        buffer[indexJ] = buffer[indexI];
                        buffer[indexI] = temp;
                    }
                }
            }

            for (int indexI = 0; indexI < MAX_BUF_SIZE; indexI++) {
                if (buffer[indexI] != '\n') {
                    String str = "" + buffer[indexI] + "\n";

                    writer.write(str);
                }
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return 0;
    }

    /*****************************************************************************************************************/
    public static int compareFilesAndMerge() {
        int fileindex = 1;
        int[] mergedBuffer = new int[MAX_BUF_SIZE];
        int mergedBufferIndex = 0;
        BufferedReader readerOne = null;
        BufferedReader readerTwo = null;
        BufferedWriter finalWriter = null;
        try {
            finalWriter = new BufferedWriter(new FileWriter(outputFilePath));
        }catch (IOException e)
        {
            e.printStackTrace();
        }

        String intermediateFileOne = "interMediateFile" + fileIndex + ".txt";
        try {
            readerOne = new BufferedReader(new FileReader(intermediateFileOne));
        } catch (IOException e) {
            e.printStackTrace();
        }

        fileindex++;

        String intermediateFileTwo = "interMediateFile" + fileIndex + ".txt";
        try {
            readerTwo = new BufferedReader(new FileReader(intermediateFileTwo));
        } catch (IOException e) {
            e.printStackTrace();
        }

        String lineFromFileOne = null;
        String lineFromFileTwo = null;
        boolean readFromFileOne = true;
        boolean readFromFileTwo = true;

        while ( fileIndex < 11 ) {
            int num1 = 0;
            int num2 = 0;
            mergedBufferIndex = 0;

            while ( mergedBufferIndex < MAX_BUF_SIZE ) {

                try {

                    if (readFromFileOne)
                        lineFromFileOne = readerOne.readLine();

                    if (readFromFileTwo)
                        lineFromFileTwo = readerTwo.readLine();

                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (lineFromFileOne == null) {

                    try {
                       readerOne.close();
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }

                    fileindex++;
                    intermediateFileOne = "interMediateFile" + fileIndex + ".txt";
                    try {
                        readerOne = new BufferedReader(new FileReader(intermediateFileOne));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                } else {
                    num1 = Integer.parseInt(lineFromFileOne);
                }

                if (lineFromFileTwo == null) {

                    try {
                        readerTwo.close();
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                    fileindex++;
                    intermediateFileTwo = "interMediateFile" + fileIndex + ".txt";
                    try {
                        readerTwo = new BufferedReader(new FileReader(intermediateFileTwo));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    num2 = Integer.parseInt(lineFromFileTwo);
                }

                if (num1 < num2) {
                    mergedBuffer[mergedBufferIndex++] = num1;
                    readFromFileOne = true;
                    readFromFileTwo = false;
                } else {
                    mergedBuffer[mergedBufferIndex++] = num2;
                    readFromFileOne = false;
                    readFromFileTwo = true;

                }
            }

            for (int bufferIndex = 0; bufferIndex < MAX_BUF_SIZE; bufferIndex++) {
                if (mergedBuffer[bufferIndex] != '\n') {
                    String str = "" + mergedBuffer[bufferIndex] + "\n";

                    try {
                        finalWriter.write(str);
                    }catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        try {
            finalWriter.close();
        }catch (IOException e)
        {
            e.printStackTrace();
        }


        return 0;
    }

    public static void main(String args[]) {

        BufferedReader reader;
        //BufferedWriter writer;
        int numberOfChunksRed = 0;

        try {
            reader = new BufferedReader(new FileReader(inputFilePath));

            String line = reader.readLine();

            while (numberOfChunksRed < CHUNKS) {

                int numberOfLinesRed = 0;
                while (numberOfLinesRed < MAX_BUF_SIZE && line != null) {

                    line = reader.readLine();
                    if (line != null) {
                        buffer[numberOfLinesRed++] = Integer.parseInt(line);
                    }
                }
                numberOfChunksRed++;
                sortAndWrite();

                numberOfLinesRed = 0;

            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        compareFilesAndMerge();

    }

    /*****************************************************************************************************************/
}


