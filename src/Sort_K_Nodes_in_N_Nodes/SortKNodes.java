package Sort_K_Nodes_in_N_Nodes;

class Node {
    int data;
    Node next;
    Node(int value) {data=value; next=null;}
}

public class SortKNodes {

    Node head;

    /*********************************/
    int addElement(int value)
    {
        Node tempNode = new Node(value);
        if(null == head)
        {
            head = tempNode;
        }
        else {
            Node lastNode = head;
            while (lastNode.next != null) {
                lastNode = lastNode.next;
            }
            lastNode.next = tempNode;
        }
        return 0;
    }

    /*********************************/
    int showElements()
    {
        Node lastNode = head;
        while(lastNode != null)
        {
            System.out.println(lastNode.data);
            lastNode = lastNode.next;
        }
        return 0;
    }

    /*********************************/
    int sortElements()
    {
        Node Inode = head;
        Node Jnode = head;
        Node tempNode = new Node(0);

        while(Inode != null)
        {
            Jnode = head;
            while(Jnode != null)
            {
                System.out.println(Inode.data);
                System.out.println(Jnode.data);
                System.out.println("--");
                if( Jnode.data >= Inode.data )
                {
                    int tempData = Jnode.data;
                    Jnode.data = Inode.data;
                    Inode.data = tempData;
                }
                Jnode = Jnode.next;
            }

            Inode = Inode.next;
        }
        return 0;
    }

    /*********************************/

    /*********************************/
    int sortEveryKElements(int k)
    {

        int icount = 0;
        int jcount = 0;
        Node startNode = head;
        Node endNode = null;
        Node KthNode = null;
        Node Inode;
        Node Jnode;

        while ( startNode !=null  ) {

            int nextEndNodeCount = 0;

            if( startNode == head )
            {
                startNode = head;
                endNode = head;
                while(nextEndNodeCount < k && endNode!= null ) {
                    endNode = endNode.next;
                    nextEndNodeCount ++;

                }
                Inode = startNode;
                KthNode = endNode;
            }

            else
            {
                startNode = endNode;
                while(nextEndNodeCount < k && endNode!= null ){
                    endNode= endNode.next;
                    nextEndNodeCount ++;

                }

                Inode = startNode;
                KthNode = endNode;
            }

            while (Inode != KthNode ) {
                Jnode = Inode;

                while (Jnode != KthNode ) {
                    System.out.println(Inode.data);
                    System.out.println(Jnode.data);
                    System.out.println("--");
                    if (Jnode.data <= Inode.data) {
                        int tempData = Jnode.data;
                        Jnode.data = Inode.data;
                        Inode.data = tempData;
                    }
                    Jnode = Jnode.next;

                }

                Inode = Inode.next;

            }
            startNode = KthNode;
        }
        return 0;
    }

    /*********************************/
    public static void main(String args[])
    {
        SortKNodes linklist = new SortKNodes();

        linklist.addElement(3);
        linklist.addElement(2);
        linklist.addElement(1);
        linklist.addElement(3);
        linklist.addElement(2);
        linklist.addElement(1);
        linklist.addElement(3);
        linklist.addElement(2);
        linklist.addElement(1);
        System.out.println("Before Sort");
        linklist.showElements();
        //linklist.sortElements();
        linklist.sortEveryKElements(2);
        System.out.println("After Sort");
        linklist.showElements();
    }
}