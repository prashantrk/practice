package BinarySearchSortedRotated;

import java.util.ArrayList;
import java.util.List;

import static BinarySearchSortedRotated.SortedRotatedArrayBinarySearch.findPivots;

public class SortedRotatedArrayBinarySearch {

    public static int pivotIndex;
    public static int pivotData;
    static ArrayList<Integer> sortedRotatedArray =  new ArrayList<Integer>(10);

    SortedRotatedArrayBinarySearch()
    {
        pivotIndex = 0;
        pivotData = 0;
        sortedRotatedArray.add(4);
        sortedRotatedArray.add(5);
        sortedRotatedArray.add(6);
        sortedRotatedArray.add(7);
        sortedRotatedArray.add(8);
        sortedRotatedArray.add(9);
        sortedRotatedArray.add(10);
        sortedRotatedArray.add(1);
        sortedRotatedArray.add(2);
        sortedRotatedArray.add(3);

    }

    static void  findPivots(int start, int end)
    {
        if(start == end)
        {
            pivotIndex = start;
            pivotData = sortedRotatedArray.get(start);
            return;
        }
        else
        {
            int mid = ( end - start ) / 2;
            if( sortedRotatedArray.get(start)  < sortedRotatedArray.get(mid) &&
                    sortedRotatedArray.get(mid)  > sortedRotatedArray.get(end) ){
                System.out.println("Temp");
                findPivots(start, mid-1);

            }
        }

    }

    public static void main(String args[])
    {
        SortedRotatedArrayBinarySearch rotatedArray = new SortedRotatedArrayBinarySearch();
        System.out.println("Actual ArrayList\n");
        for(int index=0; index<rotatedArray.sortedRotatedArray.size(); index++) {
            System.out.println(rotatedArray.sortedRotatedArray.get(index));
        }

        findPivots(0, sortedRotatedArray.size());
    }



}
