package MultipleThread;

//
//class Hi implements Runnable {
//    public void run()
//    {
//        for (int index = 0; index < 10; index++ )
//        {
//            System.out.println("Hi");
//            try {
//                Thread.sleep(1000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//    }
//}

//
//class Hello implements  Runnable {
//    public void run()
//    {
//        for (int i = 0; i < 10; i++ )
//        {
//            System.out.println("Hello");
//            try {
//                Thread.sleep(1000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//
//    }
//}

public class Hi {

    public static void main(String[] args) throws InterruptedException

    {

        Thread t1 = new Thread(()-> {
            for (int index = 0; index < 10; index++ )
            {
                System.out.println("Hi");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        Thread t2 = new Thread(()->{
            for (int i = 0; i < 10; i++ )
            {
                System.out.println("Hello");
                try {
                    Thread.sleep(900);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        });
        t2.setPriority(10);
        t1.setPriority(1);
        t1.start();
        t2.start();
        t1.join();
        t2.join();
    }
}